// Angular
import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
	selector: 'app-page',
	templateUrl: './page.component.html',
	styleUrls: ['./page.component.scss'],
})
export class PageComponent implements OnInit, AfterViewInit {
	constructor() {
	}

	/**
	 * On init
	 */
	ngOnInit(): void {
	}

	ngAfterViewInit(){
		$.getScript('/assets/js/plugin.js');
		$.getScript('/assets/js/main.js');
		$.getScript('/assets/js/custom-nav.js');
	}
}