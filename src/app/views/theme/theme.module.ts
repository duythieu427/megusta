// Angular
import { NgModule } from '@angular/core';
// Components
import { PageComponent } from './page/page.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PagesModule } from '../pages/pages.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PartialsModule } from '../partials/partials.module';
import { GetCodeComponent } from '../pages/get-code/get-code.component';


@NgModule({
	declarations: [
		PageComponent,
		HeaderComponent,
		FooterComponent,
		GetCodeComponent
	],
	exports: [
		PageComponent,
		HeaderComponent,
		FooterComponent,
		GetCodeComponent
	],
	providers: [
		
	],
	imports: [
		CommonModule,
		RouterModule,
		PagesModule,
		PartialsModule,
	]
})
export class ThemeModule {
}
