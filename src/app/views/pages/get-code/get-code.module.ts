// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Module
import { PartialsModule } from '../../partials/partials.module';
import { ThemeModule } from '../../theme/theme.module';
// Components
import { GetCodeComponent } from './get-code.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// Services

@NgModule({
	imports: [
    	CommonModule,
    	ThemeModule,
		PartialsModule,
		RouterModule.forChild([
			{
				path: '',
				component: GetCodeComponent
			},
		]),
	],
	exports: [RouterModule],
	providers: [
	],
	declarations: [
		GetCodeComponent,
	]
})
export class GetCodeModule {
}
