// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
// Module
import { ToastrService } from 'ngx-toastr';
// Components
import { BaseComponent } from '../base.component';
// Model
import { ProductModel } from 'src/app/core/models';
import { SearchProductModel } from 'src/app/core/models/search';
// Services
import { ProductService } from 'src/app/core/services';
import { Validators, FormBuilder } from '@angular/forms';


@Component({
	selector: 'get-code',
	templateUrl: './get-code.component.html',
})
export class GetCodeComponent extends BaseComponent implements OnInit {
	public products: ProductModel;  
	search: any;
	
	constructor(
		protected _router: Router,
		protected _route: ActivatedRoute,
		protected _location: Location,
		protected _toastr: ToastrService,
		private fb: FormBuilder,
	) {
		super(_router, _route, _location, _toastr);
	}

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.search = new SearchProductModel();

		this.createForm();
	}

	createForm() {
		this.form = this.fb.group({
			phone: [null, Validators.required],
		});
	}

}

