// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Module
import { PartialsModule } from '../../partials/partials.module';
import { ThemeModule } from '../../theme/theme.module';
// Components
import { HomeComponent } from './home.component';
// Services
import { ProductService } from 'src/app/core/services';

@NgModule({
	imports: [
    	CommonModule,
    	ThemeModule,
		PartialsModule,
		RouterModule.forChild([
			{
				path: '',
				component: HomeComponent
			},
		]),
	],
	exports: [RouterModule],
	providers: [
		ProductService
	],
	declarations: [
		HomeComponent,
	]
})
export class HomeModule {
}
