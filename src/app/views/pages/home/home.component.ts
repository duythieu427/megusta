// Angular
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
// Module
import { ToastrService } from 'ngx-toastr';
// Components
import { BaseComponent } from '../base.component';
// Model
import { ProductModel } from 'src/app/core/models';
import { SearchProductModel } from 'src/app/core/models/search';
// Services
import { ProductService } from 'src/app/core/services';


@Component({
	selector: 'home',
	templateUrl: './home.component.html',
})
export class HomeComponent extends BaseComponent implements OnInit {
	public products: ProductModel;  
	search: any;
	
	constructor(
		protected _router: Router,
		protected _route: ActivatedRoute,
		protected _location: Location,
		protected _toastr: ToastrService,

		private productService: ProductService
	) {
		super(_router, _route, _location, _toastr);
	}

	/**
	 * On init
	 */
	ngOnInit(): void {
		this.search = new SearchProductModel();
	}

	// findAll(): any {
	// 	try {
	// 	  this.productService.findAll(false, this.search)
	// 		.then((response) => {
	// 		  this.products = response;
	// 		  console.log(this.products);
	// 		})
	// 		.catch((error) => {
	// 		  console.log(error)
	// 		});
	// 	} catch (error) {
	// 	  console.log(error)
	// 	}
	// }
}

