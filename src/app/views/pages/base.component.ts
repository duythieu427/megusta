/**
 * Created by duythieu on 2020.
 */
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
// import { PAGINATION, SESSION, SORT_TYPE } from '../modules/constants';
// import { UtilHelper } from '../helpers/util.helper';
// import { UserModel } from '../models';
// import { UserIdentityService } from '../services/user_identity.service';

// import { ROLE } from '../modules/constants';
import _ from 'lodash';
import { UserIdentityService } from 'src/app/core/services';
import { UserModel } from 'src/app/core/models';
import { FormGroup } from '@angular/forms';
// import { PrintHelper } from 'app/helpers';
import { DateHelper, UtilHelper } from 'src/app/core/helpers';
import { ToastrService } from 'ngx-toastr';


export class BaseComponent {
  public profile: UserModel;
  public dataSource: any;
  public filterArray: any;
  public dateRangeDefault = { 'begin': Date, 'end': Date };
  public currentDay = DateHelper.currentDay();
  public date: Date;

  protected pageTitle = '';
  protected totalItems: number = 0;
  protected currentPage: number = 1;

  protected selectedId = [];
  protected bulkAll: boolean = false;

  protected search: any;
  protected isShowPassword: boolean = false;
  protected isShowPasswordAgain: boolean = false;
  protected isShowOldPassword: boolean = false;
  protected currentLocation: string = '';
  protected currentLanguage: any;
  protected isASC: boolean = false;

  /** form */
  public form: FormGroup;

  constructor(
    protected _router: Router,
    protected _route: ActivatedRoute,
    protected _location: Location,
    protected _toastr: ToastrService,
  ) {
    this.currentLocation = this._router.url;
    if (UserIdentityService.getProfile()) {
      this.profile = UserIdentityService.getProfile();
    } else {
      this.profile = new UserModel();
    }
  }

  public initSearch() {
    _.forOwn(this._route.snapshot.queryParams, (value, key) => {
      if (value) {
        if (key === 'page') {
          this.currentPage = parseInt(value);
        }
        this.search[key] = value;
      }
    });
  }

  public back() {
    this._location.back();
  }

  /**
   *
   * @param route
   */
  public navigate(route: any[], queryParams: any = {}): any {
    this._router.navigate(route, { queryParams: queryParams });
  }

  /**
   *
   * @param route
   */
  public navigateByUrl(url: string): any {
    this._router.navigateByUrl(url);
  }

  /**
   * Get List Data
   * @param offset
   */
  public findAll(search: any = null, offset: number = 0): any {
  }

  public setUrl() {
    const url = this.currentLocation.split('?')[0].split('#')[0];
    this._location.replaceState(url + '?' + UtilHelper.parseFilterToStringNoEmpty(this.search));
  }

  clearDate(event) {
    event.stopPropagation();
    this.date = null;
    this.search.fromTime = '';
    this.search.toTime = '';
    this.findAll();
  }

  /**
   * Filter data
   * @param $event
   */
  // public filter($event): any {
  //   const name = $event.target.name;
  //   const value = $event.target.value;
  //   this.search[name] = value;
  //   this.pageChanged(1);
  // }

  applyFilter($event): any{
    const name = $event.target.name;
    const value = $event.target.value;
    this.search[name] = value;
    this.search.page = this.currentPage;
    this.findAll(this.search, 0);
    this.setUrl();
    
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  filterLocal(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    // fiter nested object
    this.dataSource.filterPredicate = (data: any, filter) => {
      const dataStr =JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) != -1; 
    }

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /**
   * Set success message
   * @param message
   */
  // public setCreate(message: string): any {
  //   this._toastr.showActionNotification(message, MessageType.Create);
  // }

  /**
   * Set update message
   * @param message
   */
  // public setUpdate(message: string): any {
  //   this._toastr.showActionNotification(message, MessageType.Update);
  // }

  /**
   * Set delete message
   * @param message
   */
  // public setDelete(message: string): any {
  //   this._toastr.showActionNotification(message, MessageType.Delete);
  // }

  /**
   * Set error message
   * @param message
   */
  // public setError(error: any = {}): any {
  //   if (error.message) {
  //     this._toastr.showActionNotification(error.message, MessageType.Error);
  //   } else {
  //     this._toastr.showActionNotification(error, MessageType.Error);
  //   }
  // }

  /**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	errorForm(controlName: string, validationType: string): boolean {
		const control = this.form.controls[controlName];
		if (!control) {
			return false;
		}

		const result =
			control.hasError(validationType) &&
			(control.dirty || control.touched);
		return result;
	}
}