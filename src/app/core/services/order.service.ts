import { JsonMapper } from '../modules/mapper/json.mapper';
import { TransferHttp } from '../modules/transfer-http/transfer-http';
import { UtilHelper } from '../helpers/util.helper';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BaseService } from './base.service';
import { OrderModel, ResponseModel } from '../models';
import { REST_API } from '../modules/constants';

@Injectable()
export class OrderService extends BaseService {

    constructor(public http: TransferHttp) {
        super(http);
    }

    /**
     * Get list variable
     * @returns {Promise<T>|Promise<U>}
     */
    public findAll(paging: boolean = true, filter: any = {}): Promise<any> {

        if (!paging) {
            filter.offset = '';
            filter.limit = '';
        }
        const queryString = UtilHelper.parseFilterToString(filter);
        return this.makeHttpGet(`${this.apiUrl}/` + REST_API.ORDER + '?' + queryString)
            .then((res) => {
                res.data = JsonMapper.deserialize(OrderModel, res.data);
                return res;
            });
    }

      /**
     * Get detail
     * @param id
     */
    public findById(id: string): Promise<OrderModel> {
        return this.makeHttpGet(`${this.apiUrl}/${REST_API.ORDER}/${id}`)
            .then((res) => {
                return JsonMapper.deserialize(OrderModel, res);
            });
    }

    /**
     * Create
     * @param data
     */
    public create(data: OrderModel): Promise<ResponseModel> {
        return this.makeHttpPost(`${this.apiUrl}/${REST_API.ORDER}`, JsonMapper.serialize(data))
            .then(() => {
                return ResponseModel.createSuccess();
            });
    }
    /**
     * Update
     * @param data
     */

    public update(data: OrderModel): Promise<ResponseModel> {
        return this.makeHttpPut(`${this.apiUrl}/${REST_API.ORDER}/${data.id}`, JsonMapper.serialize(data))
            .then(() => {
                return ResponseModel.updateSuccess();
            });
    }

    public delete(id: string): Promise<ResponseModel> {
        return this.makeHttpDelete(`${this.apiUrl}/${REST_API.ORDER}/${id}`)
            .then(() => {
                return ResponseModel.deleteSuccess();
            });
    }

    // delete order
    public status(data: OrderModel): Promise<ResponseModel> {
        return this.makeHttpPut(`${this.apiUrl}/${REST_API.ORDER_STATUS}`, data)
            .then(() => {
                return ResponseModel.updateSuccess();
            });
    }

    // print order
    // public printHtml(data: OrderModel): Promise<ResponseModel> {
    //     return this.makeHttpPut(`${this.apiUrl}/${REST_API.ORDER}`, data)
    //         .then(() => {
    //             return ResponseModel.updateSuccess();
    //         });
    // }

    public printHtml(id: string): Promise<ResponseModel> {
        return this.makeHttpGet(`${this.apiUrl}/` + REST_API.ORDER + '/print_invoice/' + id);
    }
}