import { JsonMapper } from '../modules/mapper/json.mapper';
import { TransferHttp } from '../modules/transfer-http/transfer-http';
import { UtilHelper } from '../helpers/util.helper';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BaseService } from './base.service';
import { ResponseModel, WareHouseItemModel } from '../models';
import { REST_API } from '../modules/constants';

@Injectable()
export class WareHouseItemService extends BaseService {

    constructor(public http: TransferHttp) {
        super(http);
    }

    /**
     * Get list variable
     * @returns {Promise<T>|Promise<U>}
     */
    public findAll(paging: boolean = true, filter: any = {}): Promise<any> {

        if (!paging) {
            filter.offset = '';
            filter.limit = '';
        }
        const queryString = UtilHelper.parseFilterToString(filter);
        return this.makeHttpGet(`${this.apiUrl}/` + REST_API.WAREHOUSE_ITEM + '?' + queryString)
            .then((res) => {
                res.data = JsonMapper.deserialize(WareHouseItemModel, res.data);
                return res;
            });
    }

      /**
     * Get detail
     * @param id
     */
    public findById(id: string): Promise<WareHouseItemModel> {
        return this.makeHttpGet(`${this.apiUrl}/${REST_API.WAREHOUSE_ITEM}/${id}`)
            .then((res) => {
                return JsonMapper.deserialize(WareHouseItemModel, res);
            });
    }

    /**
     * Create
     * @param data
     */
    public create(data: WareHouseItemModel): Promise<ResponseModel> {
        return this.makeHttpPost(`${this.apiUrl}/${REST_API.WAREHOUSE_ITEM}`, JsonMapper.serialize(data))
            .then(() => {
                return ResponseModel.createSuccess();
            });
    }
    /**
     * Update
     * @param data
     */

    public update(data: WareHouseItemModel): Promise<ResponseModel> {
        return this.makeHttpPut(`${this.apiUrl}/${REST_API.WAREHOUSE_ITEM}/${data.id}`, JsonMapper.serialize(data))
            .then(() => {
                return ResponseModel.updateSuccess();
            });
    }

    public delete(id: string): Promise<ResponseModel> {
        return this.makeHttpDelete(`${this.apiUrl}/${REST_API.WAREHOUSE_ITEM}/${id}`)
            .then(() => {
                return ResponseModel.deleteSuccess();
            });
    }
}