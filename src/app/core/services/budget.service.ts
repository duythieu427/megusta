import { JsonMapper } from '../modules/mapper/json.mapper';
import { TransferHttp } from '../modules/transfer-http/transfer-http';
import { UtilHelper } from '../helpers/util.helper';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BaseService } from './base.service';
import { BudgetModel, ResponseModel } from '../models';
import { REST_API } from '../modules/constants';

@Injectable()
export class BudgetService extends BaseService {

    constructor(public http: TransferHttp) {
        super(http);
    }

    /**
     * Get list variable
     * @returns {Promise<T>|Promise<U>}
     */
    public findAll(paging: boolean = true, filter: any = {}): Promise<any> {

        if (!paging) {
            filter.offset = '';
            filter.limit = '';
        }
        const queryString = UtilHelper.parseFilterToString(filter);
        return this.makeHttpGet(`${this.apiUrl}/` + REST_API.BUDGET + '?' + queryString)
            .then((res) => {
                res.data = JsonMapper.deserialize(BudgetModel, res.data);
                return res;
            });
    }

      /**
     * Get detail
     * @param id
     */
    public findById(id: string): Promise<BudgetModel> {
        return this.makeHttpGet(`${this.apiUrl}/${REST_API.BUDGET}/${id}`)
            .then((res) => {
                return JsonMapper.deserialize(BudgetModel, res);
            });
    }

    /**
     * Create
     * @param data
     */
    public create(data: BudgetModel): Promise<ResponseModel> {
        return this.makeHttpPost(`${this.apiUrl}/${REST_API.BUDGET}`, JsonMapper.serialize(data))
            .then(() => {
                return ResponseModel.createSuccess();
            });
    }
    /**
     * Update
     * @param data
     */

    public update(data: BudgetModel): Promise<ResponseModel> {
        return this.makeHttpPut(`${this.apiUrl}/${REST_API.BUDGET}/${data.id}`, JsonMapper.serialize(data))
            .then(() => {
                return ResponseModel.updateSuccess();
            });
    }

    public delete(id: string): Promise<ResponseModel> {
        return this.makeHttpDelete(`${this.apiUrl}/${REST_API.BUDGET}/${id}`)
            .then(() => {
                return ResponseModel.deleteSuccess();
            });
    }
}