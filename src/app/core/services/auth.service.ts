import { JsonMapper } from '../modules';
import { TransferHttp } from '../modules/transfer-http/transfer-http';
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BaseService } from './base.service';
import { TokenModel } from '../models/token.model';
import { REST_API } from '../modules/constants';
import { ResponseModel } from '../models';

@Injectable()
export class AuthService extends BaseService {

  constructor(protected _http: TransferHttp) {
    super(_http);
  }

  /**
   * Login
   */

  public login(data): Promise<TokenModel> {
    const obj = JsonMapper.serialize(data);
    return this.makeHttpPost(`${this.apiUrl}/` + REST_API.AUTH.LOGIN, obj)
      .then((res) => {
        return JsonMapper.deserialize(TokenModel, res);
      });
  }

  /**
   * Register
   */

  public register(data): Promise<TokenModel> {
    const obj = JsonMapper.serialize(data);

    return this.makeHttpPost(`${this.apiUrl}/` + REST_API.AUTH.REGISTER, obj)
      .then((res) => {
        return JsonMapper.deserialize(TokenModel, res);
      });
  }

  /**
   * forgot password
   */

  public forgotPassword(phone: string): Promise<ResponseModel> {
    return this.makeHttpPost(`${this.apiUrl}/` + REST_API.AUTH.FORGOT_PASSWORD, { phone: phone});
  }

  /**
   * reset password
   */

  // public resetPassword(data: UserModel): Promise<ResponseModel> {
  //   return this.makeHttpPost(`${this.apiUrl}/` + REST_API.AUTH.RESET_PASSWORD, JsonMapper.serialize(data));
  // }


  /**
   * Logout
   */
  public logout() {
    return this.makeHttpDelete(`${this.apiUrl}/` + REST_API.AUTH.LOGOUT);
  }
}
