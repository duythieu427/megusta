import { NgModule } from '@angular/core';
import { TransferHttp } from './transfer-http';
import { HttpClientModule } from '@angular/common/http';
import { ConfigService } from '@ngx-config/core';

@NgModule({
  imports: [
    HttpClientModule
  ],
  providers: [
    TransferHttp,
    ConfigService
  ]
})
export class TransferHttpModule { }
