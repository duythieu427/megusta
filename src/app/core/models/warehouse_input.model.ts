import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';
import { PaymentModel } from './payment.model';
import { ItemsModel } from './items.model';

export class WareHouseInputModel extends BaseModel {
  @Json('brandProviderId')
  public brandProviderId: string = undefined;

  @Json('warehouseId')
  public warehouseId: string = undefined;

  @Json('totalQuantity')
  public totalQuantity: number = undefined;

  @Json('totalMoney')
  public totalMoney: number = undefined;

  @Json('payTotal')
  public payTotal: number = undefined;

  @Json('debt')
  public debt: number = undefined;

  @Json('payment')
  public payment: PaymentModel = undefined;

  @Json('items')
  public items: ItemsModel[] = undefined;

  @Json('checkCost')
  public checkCost: boolean = true;

  @Json('totalRow')
  public totalRow: number = 0;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    // this.name = '';
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}