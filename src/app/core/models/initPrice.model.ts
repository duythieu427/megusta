import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';
import { PriceOriginModel } from './priceOrigin.model';
import { PriceTransportModel } from './priceTransport.model';

export class InitPriceModel extends BaseModel {
  @Json('priceOrigin')
  public priceOrigin: PriceOriginModel = undefined;

  @Json('priceTransport')
  public priceTransport: PriceTransportModel = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}