import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';
import { ItemsModel } from './items.model';

export class WareHouseChangeModel extends BaseModel {
  @Json('toWareHouseId')
  public toWareHouseId: string = undefined;

  @Json('warehouseItemsToChange')
  public warehouseItemsToChange: ItemsModel[] = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}