import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';
import { InitPriceModel } from './initPrice.model';

export class ItemsModel extends BaseModel {
  @Json('productId')
  public productId: string = undefined;

  @Json('warehouseId')
  public warehouseId: string = undefined;

  @Json('exprireDay')
  public exprireDay: string = undefined;

  @Json('quantity')
  public quantity: number = undefined;

  @Json('initPrice')
  public initPrice: InitPriceModel = undefined;

  @Json('discount')
  public discount: number = undefined;

  @Json('totalPrice')
  public totalPrice: number = 0;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    // this.name = '';  
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}