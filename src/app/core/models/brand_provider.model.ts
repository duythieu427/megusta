import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';

export class BrandProviderModel extends BaseModel {
  @Json('name')
  public name: string = undefined;

  @Json('phone')
  public phone: string = undefined;
  
  @Json('province')
  public province: string = undefined;

  @Json('address')
  public address: string = undefined;

  @Json('email')
  public email: string = undefined;

  @Json('fax')
  public fax: string = undefined;

  @Json('desc')
  public desc: string = undefined;

  @Json('isBrand')
  public isBrand: boolean = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    this.name = '';
    this.phone = '';
    this.province = '';
    this.address = '';
    this.email = '';
    this.fax = '';
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}