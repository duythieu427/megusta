import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';
import { ProductModel } from './product.model';
import { InitPriceModel } from './initPrice.model';

export class WareHouseItemModel extends BaseModel {
  @Json('productId')
  public productId: string = undefined;
  
  @Json('name')
  public name: string = undefined;

  @Json('warehouseId')
  public warehouseId: string = undefined;

  @Json('warehouseInputId')
  public warehouseInputId: string = undefined;

  @Json('exprireDay')
  public exprireDay: string = undefined;

  @Json('quantity')
  public quantity: number = undefined;

  @Json('initPrice')
  public initPrice: InitPriceModel = undefined;

  @Json('product')
  public product: ProductModel = undefined;

  @Json('isPriceOrigin')
  public isPriceOrigin: boolean = undefined;

  @Json('total')
  public total: number = undefined;

  @Json('totalRow')
  public totalRow: number = 0;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    // this.name = '';
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}