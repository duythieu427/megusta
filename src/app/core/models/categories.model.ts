import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';

export class CategoriesModel extends BaseModel {
  @Json('name')
  public name: string = undefined;

  @Json('des')
  public des: string = undefined;

  @Json('parentId')
  public parentId: string = undefined;

  @Json('priority')
  public priority: number = undefined;
  
  @Json('orderTotalQuantity')
  public orderTotalQuantity: number = undefined;

  @Json('orderTotalPrice')
  public orderTotalPrice: number = undefined;

  @Json('orderTotalDiscount')
  public orderTotalDiscount: number = undefined;

  @Json('orderTotal')
  public orderTotal: number = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    this.name = '';
    this.des = '';
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}