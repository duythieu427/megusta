import { BaseSearchModel } from './base.search.model';

export class SearchWareHouseItemModel extends BaseSearchModel {
  public fromTime: string = '';
  public toTime: string = '';
  public warehouseId: string = '';
  public categoryId: string = '';
  public isTimeByExpire: boolean = true;
  public isDeleted: number = 0;
}