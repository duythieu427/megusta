import { BaseSearchModel } from './base.search.model';

export class SearchWareHouseModel extends BaseSearchModel {
  public fromTime: string = '';
  public toTime: string = '';
  public isSystem: boolean = true;
  public roleId: string = '';
}