import { BaseSearchModel } from './base.search.model';

export class SearchOrderModel extends BaseSearchModel {
  public fromTime: string = '';
  public toTime: string = '';
}