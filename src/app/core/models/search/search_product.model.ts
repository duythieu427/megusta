import { BaseSearchModel } from './base.search.model';

export class SearchProductModel extends BaseSearchModel {
  public isSystem: boolean = true;
  public categoryId: string = '';
  public fromTime: string = '';
  public toTime: string = '';
}