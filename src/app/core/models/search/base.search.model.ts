/**
 * Created by duy thieu on 2020.
 */
import { PAGINATION } from '../../modules/constants';

export class BaseSearchModel {
  public offset: number = 0;
  public key: string = '';
  // public limit: number = PAGINATION.ITEMS_PER_PAGE;
  // public sortBy: string = '';
  // public sortType: string = 'DESC';
  // public page: number = 1;
}
