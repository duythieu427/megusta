import { BaseSearchModel } from './base.search.model';

export class SearchServicesModel extends BaseSearchModel {
  public fromTime: string = '';
  public toTime: string = '';
}