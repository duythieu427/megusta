import { BaseSearchModel } from './base.search.model';

export class SearchWareHouseInputModel extends BaseSearchModel {
  public fromTime: string = '';
  public toTime: string = '';
  public brandProvider: string = undefined;
}