import { BaseSearchModel } from './base.search.model';

export class SearchBudgetModel extends BaseSearchModel {
  public isIn: boolean = undefined;
  public fromTime: string = '';
  public toTime: string = '';
}