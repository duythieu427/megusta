import { BaseSearchModel } from './base.search.model';

export class SearchUserModel extends BaseSearchModel {
  public fromTime: string = '';
  public toTime: string = '';
  public isSystem: boolean = true;
  public roleId: string = '';
}