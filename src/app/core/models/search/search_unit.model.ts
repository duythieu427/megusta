import { BaseSearchModel } from './base.search.model';

export class SearchUnitModel extends BaseSearchModel {
  public fromTime: string = '';
  public toTime: string = '';
}