import { BaseSearchModel } from './base.search.model';

export class SearchBillModel extends BaseSearchModel {
  public fromTime: string = '';
  public toTime: string = '';
  public productId: string = '';
  public creatorId: string = '';
  public customerId: string = '';
}