import { BaseSearchModel } from './base.search.model';

export class SearchCategoryModel extends BaseSearchModel {
  public isSystem: boolean = true;
  public fromTime: string = '';
  public toTime: string = '';
}