import { BaseModel } from './base.model';
import { Json } from '../modules';

export class WareHouseItemReportModel extends BaseModel {
  @Json('totalQuantityBefore')
  public totalQuantityBefore: number = undefined;

  @Json('totalPriceBefore')
  public totalPriceBefore: number = undefined;

  @Json('totalQuantityInput')
  public totalQuantityInput: number = undefined;

  @Json('totalPriceInput')
  public totalPriceInput: number = undefined;

  @Json('totalQuantityOutput')
  public totalQuantityOutput: number = undefined;

  @Json('totalPriceOutput')
  public totalPriceOutput: number = undefined;

  @Json('totalQuantityAfter')
  public totalQuantityAfter: number = undefined;

  @Json('totalPriceAfter')
  public totalPriceAfter: number = undefined;

}