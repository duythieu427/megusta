import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';

export class ServicesModel extends BaseModel {
  @Json('name')
  public name: string = undefined;

  @Json('price')
  public price: string = undefined;

  @Json('valueUnit')
  public valueUnit: string = undefined;

  @Json('value')
  public value: string = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    this.name = '';
    this.price = null;
    this.valueUnit = '';
    this.value = '';
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}