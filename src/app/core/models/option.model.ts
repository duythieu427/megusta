/**
 * Created by duythieu on 2020.
 */
import { BaseModel } from './base.model';
import { Json } from '../modules/mapper/json.decorator';
import { UtilHelper } from '../helpers';
import { ROLE, ORDERS_STATUS, PAYMENT_TYPE, LOCATION } from '../modules';

export class OptionModel extends BaseModel {
  @Json('title')
  public title: any = undefined;

  @Json('keyword')
  public keyword: any = undefined;

  @Json('isChecked')
  public isChecked: boolean = false;

  constructor() {
    super();
  }

  /**
   *
   * @param _title
   * @param _keyword
   * @returns {OptionModel}
   */
  public static init(_title: any, _keyword: any): OptionModel {
    const ret = new OptionModel();
    ret.title = UtilHelper.translate(_title);
    ret.keyword = _keyword;

    return ret;
  }

  public static getListRole(): OptionModel[] {

    const ret: OptionModel[] = [];
    ret.push(this.init('System', ROLE.SYSTEM_ADMIN));
    ret.push(this.init('Admin', ROLE.ADMIN));
    ret.push(this.init('Nhân viên kho', ROLE.WAREHOUSE));
    ret.push(this.init('Nhân viên thu ngân', ROLE.CASHIER));
    ret.push(this.init('Nhân viên bán hàng', ROLE.SALE));
    ret.push(this.init('Kế toán của hàng', ROLE.ACCOUNTANT));
    ret.push(this.init('Quản lý cửa hàng', ROLE.STOREMANAGER));
    ret.push(this.init('Shipper', ROLE.SHIPPER));
    ret.push(this.init('Khách hàng', ROLE.USER));

    return ret;
  }

  public static getListRoleMember(): OptionModel[] {

    const ret: OptionModel[] = [];
    ret.push(this.init('System', ROLE.SYSTEM_ADMIN));
    ret.push(this.init('Admin', ROLE.ADMIN));
    ret.push(this.init('Nhân viên kho', ROLE.WAREHOUSE));
    ret.push(this.init('Nhân viên thu ngân', ROLE.CASHIER));
    ret.push(this.init('Nhân viên bán hàng', ROLE.SALE));
    ret.push(this.init('Kế toán của hàng', ROLE.ACCOUNTANT));
    ret.push(this.init('Quản lý cửa hàng', ROLE.STOREMANAGER));

    return ret;
  }

  public static getListStatusOrder(): OptionModel[] {

    const ret: OptionModel[] = [];
    ret.push(this.init('Đặt hàng', ORDERS_STATUS.ORDER_BOOKED));
    ret.push(this.init('Xác nhận đơn', ORDERS_STATUS.ORDER_CONFIRM));
    ret.push(this.init('Đang giao hàng', ORDERS_STATUS.ORDER_IN_DELIVERY));
    ret.push(this.init('Xác nhận của khách hàng', ORDERS_STATUS.ORDER_CONFIRM_BY_CUSTOMER));
    ret.push(this.init('Hoàn thành', ORDERS_STATUS.ORDER_COMPLETE));
    ret.push(this.init('Đã huỷ', ORDERS_STATUS.ORDER_CANCEL));

    return ret;
  }

  public static getListPaymentType(): OptionModel[] {

    const ret: OptionModel[] = [];
    ret.push(this.init('Tiền mặt', PAYMENT_TYPE.CASH));
    ret.push(this.init('Thẻ ATM', PAYMENT_TYPE.ATM));
    ret.push(this.init('Thẻ tín dụng', PAYMENT_TYPE.CREDIT_CARD));

    return ret;
  }

  public static getListLocation(): OptionModel[] {

    const ret: OptionModel[] = [];
    ret.push(this.init('TP.HCM', LOCATION.HCM));

    return ret;
  }

}
