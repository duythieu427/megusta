import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';

export class PaymentModel extends BaseModel {
  @Json('des')
  public des: string = undefined;

  @Json('creatorId')
  public creatorId: string = undefined;

  @Json('userId')
  public userId: string = undefined;

  @Json('address')
  public address: string = undefined;

  @Json('paper')
  public paper: string = undefined;

  @Json('phone')
  public phone: string = undefined;

  @Json('reason')
  public reason: string = undefined;

  @Json('totalMoney')
  public totalMoney: number = undefined;

  @Json('payTotal')
  public payTotal: number = undefined;

  @Json('isIn')
  public isIn: boolean = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}