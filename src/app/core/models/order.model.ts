import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';
import { ItemsModel } from './items.model';
import { UserModel } from '../models';
import { BillModel } from './bill.model';

export class OrderModel extends BaseModel {
  @Json('userId')
  public userId: string = undefined;

  @Json('creatorId')
  public creatorId: string = undefined;

  @Json('creator')
  public creator: UserModel = undefined;

  @Json('items')
  public items: ItemsModel[] = undefined;

  @Json('orderStatus')
  public orderStatus: string = undefined;

  @Json('bill')
  public bill: BillModel = undefined;

  @Json('paymentType')
  public paymentType: string = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
  }

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}