import { BaseModel } from './base.model';
import { Json } from '../modules';

export class ShippingModel extends BaseModel {
  @Json('name')
  public name: string = undefined;

  @Json('phone')
  public phone: number = undefined;

  @Json('address')
  public address: string = undefined;

  @Json('isDefault')
  public isDefault: boolean = undefined;

  @Json('userId')
  public userId: string = undefined;
}