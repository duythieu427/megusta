import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';

export class WareHouseModel extends BaseModel {
  @Json('name')
  public name: string = undefined;

  @Json('phone')
  public phone: string = undefined;
  
  @Json('province')
  public province: string = undefined;

  @Json('address')
  public address: string = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    this.name = '';
    this.phone = '';
    this.province = '';
    this.address = '';
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}