import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';

export class PriceOriginModel extends BaseModel {
  @Json('brandProviderId')
  public brandProviderId: string = undefined;

  @Json('priceLevel1')
  public priceLevel1: number = undefined;

  @Json('priceLevel2')
  public priceLevel2: number = undefined;

  @Json('initPrice')
  public initPrice: number = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}