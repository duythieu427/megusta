import { INPUT_PASSWORD_MAXLENGTH } from './../modules/constants';
import { JsonMapper } from '../modules/mapper/json.mapper';
import { BaseModel } from './base.model';
import {
  INPUT_MAXLENGTH,
} from '../modules/constants';
import { ValidateModel } from './validate.model';
import { Json } from '../modules';
import { ShippingModel } from './shipping.model';

export class UserModel extends BaseModel {
  @Json('phoneEmail')
  public phoneEmail: string = undefined;

  @Json('name')
  public name: string = undefined

  @Json('email')
  public email: string = undefined

  @Json('roleIds')
  public roleIds: string[] = [];

  @Json('password')
  public password: string = undefined

  @Json('phoneNumber')
  public phoneNumber: string = undefined

  @Json('address')
  public address: string = undefined

  @Json('passport')
  public passport: string = undefined

  @Json('desc')
  public desc: string = undefined

  @Json('isEnable')
  public isEnable: boolean = true

  @Json('orderBillCount')
  public orderBillCount: number = undefined;

  @Json('orderTotalQuantity')
  public orderTotalQuantity: number = undefined;

  @Json('orderTotalPrice')
  public orderTotalPrice: number = undefined;

  @Json('orderTotalDiscount')
  public orderTotalDiscount: number = undefined;

  @Json('orderTotal')
  public orderTotal: number = undefined;
  
  @Json('orderTotalPay')
  public orderTotalPay: number = undefined;

  @Json('shippings')
  public shippings: ShippingModel[] = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
  }

  /**
   * init validate rule
   * @returns {ValidateModel}
   */
  public initValidateRules(): ValidateModel {
    // this.addRule('userName', 'required', true, this._t('Tên đăng nhập không đươc để trống.'));

    // this.addRule('displayName', 'required', true, this._t('Họ và tên không đươc để trống.'));

    // this.addRule('phone', 'required', true, this._t('Số điên thoại không đươc để trống.'));
    // this.addRule('phone', 'number', true, this._t('Số điện thoaị phải là số.'));
    // this.addRule('role', 'required', true, this._t('Nhóm người dùng không đươc để trống.'));

    this.addRule('email', 'required', true, this._t('Email không đươc để trống.'));
    this.addRule('email', 'formatEmail', true, this._t('Định dạng email không đúng.'));
    this.addRule('email', 'maxLength', INPUT_MAXLENGTH, this._t(`Maximum {0} characters.`, INPUT_MAXLENGTH));

    // this.addRule('pin', 'required', true, this._t('Mã xác nhận không đươc để trống.'));
    // this.addRule('oldPassword', 'required', true, this._t('Mật khẩu cũ không đươc để trống.'));
    // this.addRule('newPassword', 'required', true, this._t('Mật khẩu mới không đươc để trống.'));
    this.addRule('password', 'required', true, this._t('Mật khẩu không đươc để trống.'));
    // this.addRule('passwordAgain', 'equalTo', '#password', this._t('Mật khẩu xác nhận không đúng.'));
    // this.addRule('passwordConfirm', 'equalTo', '#newPassword', this._t('Mật khẩu xác nhận không đúng.'));


    return this.getRules();
  }

  /**
   *
   * @param data
   * @returns {UserModel}
   */
  public static toProfileModel(data: UserModel): UserModel {
    let model = JsonMapper.deserialize(UserModel, data);
    delete model.validateRules; // remove object validate rules
    return model;
  }

}