// import { UserModel } from './user.model';
import { BaseModel } from './base.model';
import { Json } from '../modules';
import { UserModel } from '../models';

export class TokenModel extends BaseModel {
  @Json('token')
  public token: string = undefined;

  @Json('firebaseToken')
  public firebaseToken: string = undefined;

  @Json('expiredTime')
  public expiredTime: number = undefined;

  @Json('userId')
  public userId: string = undefined;

  @Json('role')
  public role: string = undefined;

  @Json({ name: 'user', clazz: UserModel, omitEmpty: true })
  public user: UserModel = undefined;
}
