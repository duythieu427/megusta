import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';
import { WareHouseItemReportModel } from './warehouse_item_report.model';
import { CategoriesModel } from './categories.model';

export class ProductModel extends BaseModel {
  @Json('name')
  public name: string = undefined;

  @Json('unitId')
  public unitId: string = undefined;

  @Json('unit')
  public unit: string = undefined;

  @Json('price')
  public price: number = undefined;

  @Json('orderByRemaining')
  public orderByRemaining: boolean = true;

  @Json('categoryId')
  public categoryId: string = undefined;

  @Json('category')
  public category: CategoriesModel = undefined;

  @Json('brandId')
  public brandId: string = undefined;

  @Json('providerId')
  public providerId: string = undefined;
  
  @Json('warehouseItemReport')
  public warehouseItemReport: WareHouseItemReportModel = undefined;

  public clear() {
    this.name = '';
    this.unit = '';
    this.price = null;
    this.categoryId = '';
    this.orderByRemaining = true
	}


  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}