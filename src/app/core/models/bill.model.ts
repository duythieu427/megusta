import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';

export class BillModel extends BaseModel {
  @Json('name')
  public name: string = undefined;

  @Json('date')
  public date: string = undefined;

  @Json('code')
  public code: string = undefined;

  @Json('quantity')
  public quantity: number = undefined;

  @Json('total')
  public total: number = undefined;

  @Json('discount')
  public discount: number = undefined;

  @Json('pay')
  public pay: number = undefined;

  @Json('paymentType')
  public paymentType: string = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}