import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';

export class UnitModel extends BaseModel {
  @Json('name')
  public name: string = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    this.name = '';
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}