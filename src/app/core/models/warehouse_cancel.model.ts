import { BaseModel } from './base.model';
import { Json } from '../modules';
import { ValidateModel } from './validate.model';
import { ItemsModel } from './items.model';

export class WareHouseCancelModel extends BaseModel {
  @Json('warehouseId')
  public warehouseId: string = undefined;

  @Json('items')
  public items: ItemsModel[] = undefined;

  constructor() {
    super();
    // this.validateRules = new ValidateModel();
    // this.initValidateRules();
    
  }

  public clear() {
    // this.name = '';
	}

  public initValidateRules(): ValidateModel {
    this.addRule('name', 'required', true, this._t('Tên không đươc để trống.'));
    
    return this.getRules();
  }
}