// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Components
import { AppComponent } from './app.component';
//Modules
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TransferHttpModule } from './core/modules/transfer-http/transfer-http.module';
import { ConfigLoader, ConfigStaticLoader, ConfigModule } from '@ngx-config/core';
import { setting } from './core/configs/setting';
import { ToastrModule } from 'ngx-toastr';
// Partials
import { PartialsModule } from './views/partials/partials.module';
import { ThemeModule } from './views/theme/theme.module';
// Pages


export function configFactory(): ConfigLoader {
  return new ConfigStaticLoader(setting);
}


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TransferHttpModule,
    ConfigModule.forRoot(),
    FontAwesomeModule,
    ToastrModule.forRoot(),
    ThemeModule,
    PartialsModule,
  ],
  providers: [
    // ConfigModule
    {
			provide: ConfigLoader,
			useFactory: (configFactory)
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
