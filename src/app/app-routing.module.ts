// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
// Components
import { PageComponent } from './views/theme/page/page.component';
import { GetCodeComponent } from './views/pages/get-code/get-code.component';


const routes: Routes = [
	// { path: 'auth', loadChildren: "app/views/pages/auth/auth.module#AuthModule" },

	{
		path: '',
		component: PageComponent,
		// canActivate: [AuthGuard],
		children: [
			{
				path: "trang-chu",
				loadChildren: "./views/pages/home/home.module#HomeModule"
			},
			{
				path: "get-code",
				component: GetCodeComponent,
				// loadChildren: "./views/pages/get-code/get-code.module#GetCodeModule"
			},
			
			// { path: 'error/:type', component: ErrorPageComponent },
			// { path: '', redirectTo: 'trang-chu', pathMatch: 'full' },
			{ path: '**', redirectTo: 'trang-chu', pathMatch: 'full' }
		]
	},

	// { path: '**', redirectTo: 'error/403', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
